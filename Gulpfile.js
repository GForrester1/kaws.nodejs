/**
 * Created by cj on 4/16/18.
 */
const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const $ = require('gulp-load-plugins')();
const _ = require('lodash');
const sourcemaps = require('gulp-sourcemaps');
const replace = require('gulp-replace');
const cleanCSS = require('gulp-clean-css');

const reload = browserSync.reload;
const is_prod_build = false;

const sources = {
    js: [
        './src/js/app/kaws.main.js',
    ],
    vendor: {
        js: [
          './node_modules/remodal/dist/remodal.min.js'  
        ],
        css: [
            './node_modules/remodal/dist/remodal.css'
        ]
    },
    sass: [
        './src/scss/*.scss',
    ],
    views: [
        './views/**/*.pug',
    ]
};


gulp.task('app:vendor:js', function() {
    return gulp.src(sources.vendor.js)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('./public/js/'))
        .pipe(reload({stream: true }))
        ;
});

gulp.task('app:vendor:css', function() {
    return gulp.src(sources.vendor.css)
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./public/css/'))
        .pipe(reload({stream: true }))
        ;
});

gulp.task('app:js', function() {
    return gulp.src(sources.js)
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./public/js/'))
        .pipe(reload({stream: true }))
        ;
});

gulp.task('app:style', function() {
    return gulp.src(sources.sass)
        .pipe(sourcemaps.init())
        .pipe($.sass())
        .on('error', sass.logError)
        .pipe(is_prod_build ? cleanCSS() : sourcemaps.write('.')  )
        .pipe(gulp.dest('./public/css/'))
        .pipe(reload({stream: true }))
        ;
})

gulp.task('default', [
    'build', 
    'browser-sync'
], function () {
    gulp.watch(sources.js, ['app:js']);
    gulp.watch(sources.sass, ['app:style']);
    gulp.watch(sources.views, ['reload']);
});

gulp.task('build', [
    'app:vendor:css',
    'app:vendor:js',
    'app:style',
    'app:js',
    
], function () {
});


gulp.task('reload', function(){
    return gulp.run().reload({stream: true });
});

gulp.task('browser-sync', ['nodemon'], function() {
    // gulp.watch(source.scripts, ['scripts:app']);
    setTimeout(function () {
        browserSync.init(null, {
            proxy: "http://localhost:3003",
            // files: ["public/**/*.*"],
            // browser: "google chrome",
            port: 7000,
        });
    }, 1000);

});


gulp.task('nodemon', function (cb) {

    var started = false;

    return nodemon({
        script: './bin/www',
        watch: ["app/*"],

        env: { 'NODE_ENV': 'development' }
    }).on('start', function () {
        // to avoid nodemon being started multiple times
        // thanks @matthisk
        if (!started) {
            cb();
            started = true;
        }
    });
});






