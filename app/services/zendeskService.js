/**
 * Created by Garrett on 5/14/18.
 */

// libraries
const Promise = require('bluebird');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Zendesk = require('node-zendesk');

// models
const Order = require('../models/order');
const ddb       = require('../lib/ddb');

const config = require('../config');

const client = Zendesk.createClient({
   username: config.zendesk.USERNAME,
   token: config.zendesk.API_TOKEN,
   remoteUri: config.zendesk.SUBDOMAIN
});

const svc = module.exports = {};

svc.createTicket = function (subject, email, name, content, orderID) {
    return Order.findOne({
        where: {
            id: orderID
        }
    }).then(order => {
        let ticket = {
            "ticket": {
                "subject": subject,
                "comment": {
                    "body": content
                },
                "priority": 'normal',
                "requester": {
                    "name": name,
                    "email": email
                },
                "custom_fields": [
                    {
                        "id": 360004801114, //Order #
                        "value": order.id
                    },
                    {
                        "id": 360004747813, //Tracking #
                        "value": order.tracking_number
                    }
                ]
            }
        };
        var status = true;
        client.tickets.create(ticket,  function(err, req, result) {
            if (err) {
                status = false;
            }
            status = true;
        });
        return status; //@todo return actual success status of the ticket
    });
}