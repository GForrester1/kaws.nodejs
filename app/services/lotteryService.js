/**
 * Created by cj on 3/11/18.
 */

// libraries
const Promise = require('bluebird');
const errors  = require('../lib/errors');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

// models
const User = require('../models/user'); 
const Address = require('../models/address');
const LottoEntry = require('../models/lotto_entry');
const Campaign = require('../models/campaign');
const CampaignProduct = require('../models/campaign_product');
const ddb = require('../lib/ddb');

const config = require('../config');


const svc = module.exports = {};


svc.createEntry = function(data) {
    if(!data.terms){
        errors.termsOfService();
    }

    if(!data.product_id) {
        errors.requiredField('No product selected', 'product_id');
    }
    
    if(!data.first_name) {
        errors.requiredField('First name required', 'first_name');
    }

    if(!data.last_name) {
        errors.requiredField('Last name required', 'last_name');
    }

    if(!data.email) {
        errors.requiredField('Email required', 'email');
    }

    if(!data.phone) {
        errors.requiredField('Phone required', 'phone');
    }

    if(!data.address) {
        errors.requiredField('Address required', 'address');
    }

    if(!data.city) {
        errors.requiredField('City required', 'city');
    }

    if(!data.zip) {
        errors.requiredField('Zip required', 'zip');
    }

    if(!data.state) {
        errors.requiredField('State required', 'state');
    }



    data.country = 'US';

    return Campaign.queries.active_campagin.then(campaign => {
        if (!campaign) {
            errors.noActiveCampaign(); 
        }

        // @todo : remove hard coded campaign id
        data.campaign_id = config.current_campaign_id || campaign.id;
        
        return CampaignProduct.findOne({where : {campaign_id: data.campaign_id, product_id: data.product_id} })
    }).then(product  => {
        if (!product) {
            errors.productNotInCampaign();
        }
        return LottoEntry.validators.unique_entry(data);
    }).then(no_unique => {
        if (!no_unique) {
            errors.duplicateLottoEntry();
        }
        return LottoEntry.validators.valid_entrant(data);
    }).then(blocked => {
        if(!blocked){
            errors.blacklisted();
        }
        return User.create(data);
    }).then(user => {
        data.user_id = user.id; 
        
        return Address.create(data);
    }).then(address => {
        console.log(address.toJSON());
        return LottoEntry.create(data);
    }).then(entry => {
        return ddb.recordIpAddress(data).then(() => {
            return entry.toJSON();    
        })
    })
}