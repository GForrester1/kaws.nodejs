/**
 * Created by cj on 5/13/18.
 */
'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const errors  = require('../lib/errors');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;


const Order = require('../models/order');
const Order_Product = require('../models/order_product');
const Product = require('../models/product');
const config = require('../config');

Order.hasMany(Order_Product);
Order_Product.belongsTo(Product);

const svc = module.exports = {};


/**
 * Returns order object for order tracking.
 * @param params
 * @return {*}
 */
svc.trackOrder = function(params) {
    if(!params.id || !params.email) {
        errors.requiredField('Order number and email are both required', 'id');
    }
    
    return Order.findOne({
        where: _.pick(params, ['id','email']),
        attributes: [
            'id',
            'campaign_id',
            'email',
            'user_ip',
            'place_id',
            'shipping_cost',
            'duty',
            'tax',
            'subtotal',
            'total',
            'cc_number',
            'ship_first_name',
            'ship_last_name',
            'ship_address',
            'ship_address2',
            'ship_city',
            'ship_state',
            'ship_country',
            'ship_zip',
            'phone',
            'status',
            'tracking_number',
            'insert_time',
        ],
        include:[{
            model: Order_Product,
            include: [{
                model: Product
            }]

        }]
    }).then(data => {
      if(data){
          return data;
      }  else {
          return Promise.reject('Order could not be found');
      }
    });
}

