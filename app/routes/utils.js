/**
 * Created by cj on 4/25/18.
 */
'use strict';

const config = require('../config');
const ddb = require('../lib/ddb');

const router = module.exports = require('express').Router();

router.get('/clear-ip', (req,res,next) => {
    ddb.clearIpAddress({
        ip_address: req.ip_address,
        campaign_id: config.current_campaign_id
    }).then(data => res.status(200).json(data)).catch(err => next(err));
})