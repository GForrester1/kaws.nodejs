/**
 * Created by cj on 5/13/18.
 */
'use strict';

const router = module.exports = require('express').Router();

const svc = require('../services/orderService');
const zenSvc = require('../services/zendeskService');

router.post('/order', (req,res,next) => {
    svc.trackOrder({
        id: req.body.ref_num,
        email: req.body.email
    }).then(data => {
        res.render('track-order', {
            data
        });
    }).catch(err => {
        res.locals.notification = req.notification = ['danger',err];
        res.render(req.state == 'pre' ? 'index-old' : 'index', { title: 'Express' });
    });
    
});

router.post('/ticket', (req,res,next) => {
    return zenSvc.createTicket(req.body.subject, req.body.email,req.body.name, req.body.message, req.body.orderID).then(success => {
        res.status(200).json({state: success});
    });


});

