'use strict'; 

const router = module.exports = require('express').Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render(req.state == 'pre' ? 'index-old' : 'index', { title: 'Express', link: req.query.link});
});


/**
 * Product propfile page route
 */
router.get('/products/:slug', (req,res,next) => {
  //@todo Make product dynamic
  // res.render('product', {campaign_id: 2, product_id: 3, functions: functions});
  res.render('product', {product_id: 104});
});

router.get('/terms', (req,res,next) => {
  res.render('terms', {}); 
});

router.get('/payment', (req,res,next) => {
  res.render('payment', {}); 
});
