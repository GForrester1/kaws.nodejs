/**
 * Created by Garrett on 4/24/18.
 */
/**
 * DB Schema :

 CREATE TABLE public.address
 (
 id SERIAL,
 active SMALLINT DEFAULT 1,
 insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
 user_id    INTEGER,
 first_name VARCHAR,
 last_name  VARCHAR,
 phone      VARCHAR,
 address    VARCHAR,
 address2   VARCHAR,
 city       VARCHAR,
 state      VARCHAR,
 country    VARCHAR,
 zip        VARCHAR,
 label      VARCHAR,
 is_default boolean,
 CONSTRAINT address_pkey PRIMARY KEY (id)
 )
 WITH (
 OIDS = FALSE
 );
 */
'use strict';

const Sequelize = require('sequelize');
const db = require('../lib/db');
const user = require('./user');

const Model = module.exports = db.sequelize.define('address', {
    active      : {type: Sequelize.SMALLINT, defaultValue: 1},
    user_id     : {
        type: Sequelize.INTEGER,
        references: {
            model: user,
            key: "id"
        }
    },
    first_name  : Sequelize.STRING,
    last_name   : Sequelize.STRING,
    phone       : Sequelize.STRING,
    address     : Sequelize.STRING,
    address2    : Sequelize.STRING,
    city        : Sequelize.STRING,
    state       : Sequelize.STRING,
    country     : Sequelize.STRING,
    zip         : Sequelize.STRING,
    label       : Sequelize.STRING,
    is_default  : Sequelize.SMALLINT,
    
}, {
    defaultScope: {
        where: {
            active: 1
        }
    },
    
    // @todo : Change to `true` after adding timestamp field for soft delete (deletedAt)  
    paranoid: false,
    
    freezeTableName: true,
    underscored:true,
    
    /*
    @todo : enable after adding timestamp fields (createdAt, updatedAt) 
     */
    timestamps: false,
});
