/**
 * Created by Garrett on 5/!7/18.
 */
/**
 * DB Schema :

 CREATE TABLE public.blacklist
 (
 id SERIAL,
 active SMALLINT DEFAULT 1,
 insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
 field         INTEGER,
 value         VARCHAR,
 blacklist_id  INTEGER
 CONSTRAINT address_pkey PRIMARY KEY (id)
 )
 WITH (
 OIDS = FALSE
 );
 */
'use strict';

const Sequelize = require('sequelize');
const bluebird = require('bluebird');
const Op = Sequelize.Op;
const db = require('../lib/db');
const Order = require('./order');

const Model = module.exports = db.sequelize.define('blacklist', {
    active      : {type: Sequelize.SMALLINT, defaultValue: 1},
    field       : Sequelize.INTEGER,
    value       : Sequelize.STRING,
}, {
    defaultScope: {
        where: {
            active: 1
        }
    },
    
    // @todo : Change to `true` after adding timestamp field for soft delete (deletedAt)  
    paranoid: false,
    
    freezeTableName: true,
    underscored:true,
    
    /*
    @todo : enable after adding timestamp fields (createdAt, updatedAt) 
     */
    timestamps: false,
});

Model.fields = {
    1: 'email',
    2: 'cc_number',
    3: 'user_ip',
    4: 'place_id',
    5: 'composite',
    6: 'phone'
}

Model.validateOrder = function(order){
    let banned = false;
    let chain = Promise.resolve();
    for(let i = 1; i <= Object.keys(Model.fields).length; i++){
        if(i != 5){
            chain = chain.then(() => {
                return Model.count({
                    where: {
                        'field': i,
                        'value': {
                            [Op.iLike]: order[Model.fields[i]]
                        }
                    }
                }).then(count =>{
                    if(count) {
                        return Promise.reject(false);
                    } else {
                        return true;
                    }
                });
            });
        } else if(i == 5){
            let ship_address = {
                'address':  order.ship_address,
                'address2': order.ship_address2,
                'city':     order.ship_city,
                'zip':      order.ship_zip,
                'state':    order.ship_state,
                'country':  order.ship_country,
            };
            let bill_address = {
                'address':  order.bill_address,
                'address2': order.bill_address2,
                'city':     order.bill_city,
                'zip':      order.bill_zip,
                'state':    order.bill_state,
                'country':  order.bill_country,
            };

            chain = chain.then(() => {
                let q = `SELECT count(*) FROM blacklist WHERE field = `+i+` AND 
                (
                (LOWER(value)::json->>'address'     = LOWER('`+ship_address['address']+`')
                AND LOWER(value)::json->>'city'     = LOWER('`+ship_address['city']+`')
                AND LOWER(value)::json->>'zip'      = LOWER('`+ship_address['zip']+`')
                AND LOWER(value)::json->>'state'    = LOWER('`+ship_address['state']+`')
                AND LOWER(value)::json->>'country'  = LOWER('`+ship_address['country']+`')) 
                OR 
                (LOWER(value)::json->>'address'     = LOWER('`+bill_address['address']+`')
                AND LOWER(value)::json->>'city'     = LOWER('`+bill_address['city']+`')
                AND LOWER(value)::json->>'zip'      = LOWER('`+bill_address['zip']+`')
                AND LOWER(value)::json->>'state'    = LOWER('`+bill_address['state']+`')
                AND LOWER(value)::json->>'country'  = LOWER('`+bill_address['country']+`'))
                OR
                (LOWER(value)::json->>'address'     = LOWER('`+ship_address['address']+`')
                AND LOWER(value)::json->>'address2' = LOWER('`+ship_address['address2']+`')
                AND LOWER(value)::json->>'city'     = LOWER('`+ship_address['city']+`')
                AND LOWER(value)::json->>'zip'      = LOWER('`+ship_address['zip']+`')
                AND LOWER(value)::json->>'state'    = LOWER('`+ship_address['state']+`')
                AND LOWER(value)::json->>'country'  = LOWER('`+ship_address['country']+`')) 
                OR 
                (LOWER(value)::json->>'address'     = LOWER('`+bill_address['address']+`')
                AND LOWER(value)::json->>'address2' = LOWER('`+bill_address['address2']+`')
                AND LOWER(value)::json->>'city'     = LOWER('`+bill_address['city']+`')
                AND LOWER(value)::json->>'zip'      = LOWER('`+bill_address['zip']+`')
                AND LOWER(value)::json->>'state'    = LOWER('`+bill_address['state']+`')
                AND LOWER(value)::json->>'country'  = LOWER('`+bill_address['country']+`'))
                )`;

                console.log(q);
                return db.read.oneOrNone(q).then(row => {
                    if(row === null || row.blocks == 0){
                        return true;
                    } else {
                        return Promise.reject(false);
                    }
                });
            });
        }
    }
    return chain.catch(reason => {
        return reason;
    });
}
