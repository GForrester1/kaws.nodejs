/**
 * Created by cj on 3/13/18.
 */
/**
 * DB Schema :
 CREATE TABLE public.order_product (
 id SERIAL,
 active SMALLINT DEFAULT 1,
 insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
 order_id INTEGER,
 product_id INTEGER,
 unit_price NUMERICE(10, 2),
 quantity INTEGER DEFAULT 0,
 details VARCHAR DEFAULT null,
 clearance INTEGER DEFAULT 0,
 status VARCHAR DEFAULT 0,
 CONSTRAINT campaign_product_pkey PRIMARY KEY(id)
 )
 WITH (oids = false);
*/
'use strict';

const Sequelize = require('sequelize');
const db = require('../lib/db');

const Model = module.exports = db.sequelize.define('order_products', {
    id           : {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    active       : {type: Sequelize.SMALLINT, defaultValue: 1},
    quantity     : Sequelize.INTEGER,
    details      : Sequelize.STRING,
    unit_price   : Sequelize.REAL(10,2)
}, {
    defaultScope: {
        where: {
            active: 1
        }
    },
    // @todo : Change to `true` after adding timestamp field for soft delete (deletedAt)  
    paranoid: false,

    freezeTableName: true,
    underscored:true,

    /*
     @todo : enable after adding timestamp fields (createdAt, updatedAt) 
     */
    timestamps: false,
});