/**
 * Created by cj on 3/13/18.
 */
/**
 * @todo: consider breaking down the schema into more streamline tables.
 DB Schema:

 CREATE TABLE public.product (
 id SERIAL,
 active smallint DEFAULT 1,
 insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
 brand INTEGER,
 name VARCHAR,
 price NUMERIC(10, 2),
 description VARCHAR,
 gender VARCHAR,
 msrp NUMERIC(10, 2) NOT NULL DEFAULT 0,
 quantity INTEGER,
 slug VARCHAR,
 tags VARCHAR,
 meta_title VARCHAR,
 meta_keywords VARCHAR,
 meta_description VARCHAR,
 featured_image VARCHAR,
 condition VARCHAR,
 type VARCHAR,
 availability INTEGER,
 age_group VARCHAR,
 sub_brand INTEGER,
 length NUMERIC(10, 2),
 width NUMERIC(10, 2),
 height NUMERIC(10, 2),
 color VARCHAR,
 size VARCHAR,
 alias VARCHAR,
 options VARCHAR,
 video_link VARCHAR,
 parent_variant INTEGER DEFAULT 0,
 heel_height INTEGER,
 made_to_order smallint DEFAULT 0,
 sole VARCHAR,
 related_products VARCHAR,
 details VARCHAR,
 total_sales INTEGER DEFAULT 0,
 default_color_id INTEGER,
 linked_list VARCHAR,
 linked_name VARCHAR,
 part_number VARCHAR,
 ean VARCHAR,
 upc VARCHAR,
 remark VARCHAR,
 specs VARCHAR,
 sku VARCHAR,
 feature_name_1 VARCHAR,
 feature_name_2 VARCHAR,
 feature_1 VARCHAR,
 feature_2 VARCHAR,
 feature_image_1 VARCHAR,
 feature_image_2 VARCHAR,
 banner VARCHAR,
 feature_name_3 VARCHAR,
 feature_name_4 VARCHAR,
 feature_3 VARCHAR,
 feature_4 VARCHAR,
 display_banner smallint,
 image_alt VARCHAR,
 preorder INTEGER DEFAULT 0,
 deal_msg VARCHAR,
 sale_icon INTEGER DEFAULT 0,
 CONSTRAINT product_pkey PRIMARY KEY (id)
 )
 WITH (OIDS = FALSE)
*/
'use strict';

const Sequelize = require('sequelize');
const db = require('../lib/db');
const Campaign = require('./campaign');
const Campaign_Product = require('./campaign_product');

const Model = module.exports = db.sequelize.define('product', {
    name             : Sequelize.STRING,
    color            : Sequelize.STRING,
    featured_image   : Sequelize.STRING,
    image_alt        : Sequelize.STRING,
    details          : Sequelize.STRING,
    quantity         : Sequelize.INTEGER,
    availability     : Sequelize.INTEGER,

    height           : Sequelize.DECIMAL(10, 2),
    width            : Sequelize.DECIMAL(10, 2),
    length           : Sequelize.DECIMAL(10, 2),
    msrp             : Sequelize.DECIMAL(10, 2),
    price            : Sequelize.DECIMAL(10, 2),

    slug             : Sequelize.STRING,
    tags             : Sequelize.STRING,
    meta_title       : Sequelize.STRING,
    meta_keywords    : Sequelize.STRING,
    meta_description : Sequelize.STRING
}, {
    // @todo : Change to `true` after adding timestamp field for soft delete (deletedAt)  
    paranoid: false,

    freezeTableName: true,
    underscored:true,


    /*
     @todo : enable after adding timestamp fields (createdAt, updatedAt) 
     */
    timestamps: false,
});
const Image = db.sequelize.define('product_images',{
    image: Sequelize.STRING,
    display_order: Sequelize.INTEGER
}, {
    defaultScope: {
        where: {
            active: 1
        }
    },
    // @todo : Change to `true` after adding timestamp field for soft delete (deletedAt)
    paranoid: false,

    freezeTableName: true,
    underscored:true,

    /*
     @todo : enable after adding timestamp fields (createdAt, updatedAt)
     */
    timestamps: false,
});

Model.hasMany(Image, {as: 'Images'});