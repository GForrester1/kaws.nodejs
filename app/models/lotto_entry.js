/**
 * Created by cj on 3/11/18.
 */
/**
 * DB Schema :
 CREATE TABLE public.lotto_entries (
 id SERIAL,
 active SMALLINT DEFAULT 1,
 insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
 email_date TIMESTAMP WITHOUT TIME ZONE,
 claimed_date TIMESTAMP WITHOUT TIME ZONE,
 product_id INTEGER,
 campaign_id INTEGER,
 user_id INTEGER,
 winner SMALLINT DEFAULT 0,
 redeemed SMALLINT DEFAULT 0,
 ref_num VARCHAR,
 CONSTRAINT lotto_entries_pkey PRIMARY KEY(id)
 )
 WITH (oids = false);

 */
'use strict';

const Sequelize = require('sequelize');
const db = require('../lib/db');

const Model = module.exports = db.sequelize.define('lotto_entries', {
    active      : {type: Sequelize.SMALLINT, defaultValue: 1},
    product_id  : Sequelize.INTEGER,
    campaign_id : Sequelize.INTEGER,
    user_id     : Sequelize.INTEGER,
    winner      : {type: Sequelize.SMALLINT, defaultValue: 0},
    redeemed    : {type: Sequelize.SMALLINT, defaultValue: 0},
    ref_num     : Sequelize.STRING
}, {
    defaultScope: {
        where: {
            active: 1
        }
    },
    // @todo : Change to `true` after adding timestamp field for soft delete (deletedAt)  
    paranoid: false,

    freezeTableName: true,


    /*
     @todo : enable after adding timestamp fields (createdAt, updatedAt) 
     */
    timestamps: false,
});


Model.validators = {
    unique_entry : function({email, campaign_id}) {
        let q = `select count(lotto_entries.id) AS num_entries from 
                    lotto_entries inner join "user" on lotto_entries.user_id = "user".id and lotto_entries.active=1 and lotto_entries.campaign_id = ${campaign_id} 
                    where 
                    "user".active=1 and "user".email ilike '${email}'`;

        return db.read.oneOrNone(q).then(row => {
            return row === null || row.num_entries == 0;
        })
    },
    valid_product : function({product_id, campaign_id}) {
        let q = `select count(lotto_entries.id) AS num_entries from 
                    lotto_entries inner join "user" on lotto_entries.user_id = "user".id and lotto_entries.active=1 and lotto_entries.campaign_id = ${campaign_id} 
                    where 
                    "user".active=1 and "user".email ilike '${email}'`;

        return db.read.oneOrNone(q).then(row => {
            return row === null || row.num_entries == 0;
        })
    },
    valid_entrant : function(data) {
        let ip = data.ip_address;
        let email = data.email;
        let phone = data.phone;
        let q = `select count(*) AS blocks from 
                    blacklist WHERE 
                    (LOWER(value) LIKE LOWER('${email}') AND field = 1 ) OR 
                    (value LIKE '${ip}' AND field = 3) OR 
                    (LOWER(value) LIKE LOWER('${phone}') AND field = 6)`;

        return db.read.oneOrNone(q).then(row => {
            return row === null || row.blocks == 0;
        })
    },
};

