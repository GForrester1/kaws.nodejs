/**
 * Created by cj on 3/11/18.
 */
/**
 * DB Schema :
 
 CREATE TABLE public."user" (
 id SERIAL,
 active SMALLINT DEFAULT 1,
 insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
 email VARCHAR NOT NULL,
 password VARCHAR NOT NULL,
 hash VARCHAR,
 first_name VARCHAR,
 last_name VARCHAR,
 phone VARCHAR,
 ref_id VARCHAR,
 company_name VARCHAR,
 address VARCHAR,
 address_1 VARCHAR,
 city VARCHAR,
 state VARCHAR,
 zip VARCHAR,
 dob VARCHAR,
 memo VARCHAR,
 square_id VARCHAR,
 first_visit VARCHAR,
 trans_count INTEGER,
 total_spend VARCHAR,
 subscribe VARCHAR,
 is_stc INTEGER DEFAULT 0,
 wholesale_id INTEGER,
 CONSTRAINT user_pkey PRIMARY KEY(id)
 )
 WITH (oids = false);
 
 */
'use strict';

const Sequelize = require('sequelize');
const address = require('./address');
const db = require('../lib/db');

const Model = module.exports = db.sequelize.define('user', {
    active      : {type: Sequelize.SMALLINT, defaultValue: 1},
    first_name  : Sequelize.STRING, 
    last_name   : Sequelize.STRING,
    email       : Sequelize.STRING,
    password    : {type: Sequelize.STRING, defaultValue: '**************'} ,
    
}, {
    defaultScope: {
        where: {
            active: 1
        }
    },
    
    // @todo : Change to `true` after adding timestamp field for soft delete (deletedAt)  
    paranoid: false,
    
    freezeTableName: true,
    underscored: true,
    
    /*
    @todo : enable after adding timestamp fields (createdAt, updatedAt) 
     */
    timestamps: false,
});

Model.hasMany(address,{as: "addresses"});
