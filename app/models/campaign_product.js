/**
 * Created by cj on 3/13/18.
 */
/**
 * DB Schema :
 CREATE TABLE public.campaign_product (
 id SERIAL,
 active SMALLINT DEFAULT 1,
 insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
 campaign_id INTEGER,
 product_id INTEGER,
 quantity INTEGER DEFAULT 0,
 notify_level INTEGER DEFAULT 0,
 winner INTEGER DEFAULT 0,
 max_per_user INTEGER DEFAULT 3,
 CONSTRAINT campaign_product_pkey PRIMARY KEY(id)
 )
 WITH (oids = false);
*/
'use strict';

const Sequelize = require('sequelize');
const db = require('../lib/db');

const Model = module.exports = db.sequelize.define('campaign_product', {
    active      : {type: Sequelize.SMALLINT, defaultValue: 1},
    quantity : Sequelize.INTEGER,
    max_per_user: Sequelize.INTEGER
}, {
    defaultScope: {
        where: {
            active: 1
        }
    },
    // @todo : Change to `true` after adding timestamp field for soft delete (deletedAt)  
    paranoid: false,

    freezeTableName: true,
    underscored:true,

    /*
     @todo : enable after adding timestamp fields (createdAt, updatedAt) 
     */
    timestamps: false,
});