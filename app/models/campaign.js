/**
 * Created by cj on 3/11/18.
 */
/**
 * DB Schema :
 CREATE TABLE public.campaign (
 id SERIAL,
 active SMALLINT DEFAULT 1,
 insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
 type VARCHAR,
 name VARCHAR,
 status VARCHAR,
 c_limit INTEGER,
 CONSTRAINT campaign_pkey PRIMARY KEY(id)
 )
 WITH (oids = false);
 */
'use strict';

const Sequelize = require('sequelize');
const db = require('../lib/db');
const Product = require('./product');
const Campaign_Product = require('./campaign_product');

const Model = module.exports = db.sequelize.define('campaign', {
    active      : {type: Sequelize.SMALLINT, defaultValue: 1},
    type        : Sequelize.STRING,
    name        : Sequelize.STRING,
    status      : Sequelize.STRING, 
    c_limit     : Sequelize.INTEGER,
}, {
    defaultScope: {
        where: {
            active: 1
        }
    },
    // @todo : Change to `true` after adding timestamp field for soft delete (deletedAt)  
    paranoid: false,

    freezeTableName: true,
    underscored: true,


    /*
     @todo : enable after adding timestamp fields (createdAt, updatedAt) 
     */
    timestamps: false,
});


Model.queries = {
    active_campaign : Model.findOne({where : {status : 'Active', active: 1} })
};