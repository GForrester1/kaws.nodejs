/**
 * Created by cj on 3/11/18.
 */
const Promise = require('bluebird');
const config = require('../config');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const pgp = require('pg-promise')({
    promiseLib: Promise
});


const sequelize = config.dbr && config.dbr.length ? new Sequelize(config.dbw.database, null, null , {
    host : config.dbw.host,
    dialect : 'postgres',

    // disable logging; default: console.log
    // logging: console.log,
    logging: false,


    // preventing sequelize from inserting NULL for undefined values.
    omitNull: true,
    replication : {
        write: {
            host : config.dbw.host,
            username : config.dbw.username || config.dbw.user,
            password : config.dbw.password,
            port : config.dbw.port,
        },
        read: config.dbr.map(t=> {
            return {
                host : t.host,
                username : t.username || t.user || t.username || t.user,
                password : t.password || t.password,
                port : t.port || t.port,
            }
        })
    },

    pool : {
        max : 100 ,
        min : 10,
        idle: 10000
    }
}) : new Sequelize(config.dbw.database, config.dbw.user, config.dbw.password, {
    host : config.dbw.host,
    dialect : 'postgres',

    // disable logging; default: console.log
    logging: console.log,
    // logging: false,


    // preventing sequelize from inserting NULL for undefined values.
    omitNull: true,

    pool : {
        max : 100 ,
        min : 0,
        idle: 10000
    }
});

const svc = module.exports = {
    db : pgp(config.dbw),
    Op,
    sequelize : sequelize
};

svc.read = config.dbr && config.dbr.length ? pgp(config.dbr[0]) :svc.db; 
