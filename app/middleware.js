/**
 * Created by cj on 4/24/18.
 */
'use strict';

const router = module.exports = require('express').Router();

const config = require('./config');
const ddb = require('./lib/ddb');

router.use((req,res,next) => {
    
    // get the true ip address while accounting to Cloudflare and Nginx proxy 
    // CF-Connecting-IP is cloudflare's header
    // x-forwarded-for - industry standrad for reporting ip address by proxy (cloudflare, nginx, and other proxy clients).
    req.ip_address = req.headers['CF-Connecting-IP'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;


    
    
    const date = new Date();
    const unix = date.getTime() / 1000;
    
    const start = 1524672000;
    const end = 1524675600;

    req.state = 'active';

    if(unix < start) {
        req.state = 'pre';
    } else if(unix > end) {
        req.state = 'post';
    }


    res.locals.state = req.state = (req.query.state || null) || req.state; 
    
    
    if(req.state == 'active') {
        // check if ip already submitted for lottery in current campaign
        ddb.ipAlreadySubmitted({
            ip_address: req.ip_address,
            campaign_id: config.current_campaign_id,
        }).then(data => {

            // assign results in a local variable accessible in .pug
            res.locals.allow_lottery = !data;


            return next();
        });    
    } else  {
        return next();
    }
});

router.use((req,res,next) => {
   //Commonly used functions

    res.locals.getStates = function(){
        let states = {
            'AL': 'Alabama',
            'AK': 'Alaska',
            'AZ': 'Arizona',
            'AR': 'Arkansas',
            'CA': 'California',
            'CO': 'Colorado',
            'CT': 'Connecticut',
            'DE': 'Delaware',
            'FL': 'Florida',
            'GA': 'Georgia',
            'HI': 'Hawaii',
            'ID': 'Idaho',
            'IL': 'Illinois',
            'IN': 'Indiana',
            'IA': 'Iowa',
            'KS': 'Kansas',
            'KY': 'Kentucky',
            'LA': 'Louisiana',
            'ME': 'Maine',
            'MD': 'Maryland',
            'MA': 'Massachusetts',
            'MI': 'Michigan',
            'MN': 'Minnesota',
            'MS': 'Mississippi',
            'MO': 'Missouri',
            'MT': 'Montana',
            'NE': 'Nebraska',
            'NV': 'Nevada',
            'NH': 'New Hampshire',
            'NJ': 'New Jersey',
            'NM': 'New Mexico',
            'NY': 'New York',
            'NC': 'North Carolina',
            'ND': 'North Dakota',
            'OH': 'Ohio',
            'OK': 'Oklahoma',
            'OR': 'Oregon',
            'PA': 'Pennsylvania',
            'RI': 'Rhode Island',
            'SC': 'South Carolina',
            'SD': 'South Dakota',
            'TN': 'Tennessee',
            'TX': 'Texas',
            'UT': 'Utah',
            'VT': 'Vermont',
            'VA': 'Virginia',
            'WA': 'Washington',
            'WV': 'West Virginia',
            'WI': 'Wisconsin',
            'WY': 'Wyoming',
            'DC': 'District of Columbia',
            'GU': 'Guam',
            'PR': 'Puerto Rico',
            'VI': 'Virgin Islands'};
        return states;
    }
    return next();
});