/**
 * Created by cj on 4/24/18.
 */
'use strict';

const svc = require('../app/lib/ddb');
const shortid = require('shortid');

describe('testing DynamoDb service / lib', () => {

    const in_data = {
        ip_address: shortid(),
        campaign_id: 5
    };
    
    it('should add a new entry', done => {
        svc.recordIpAddress(in_data).then(data => {
            done();
        }).catch(err => {
            done(err)
        });
    })

    it('check ip address', done => {
        svc.checkIpAddress(in_data).then(data => {
            done();
        }).catch(err => {
            done(err)
        });
    })

    it('check ip address exists (bool)', done => {
        svc.ipAlreadySubmitted(in_data).then(data => {
            console.log('data :', data);
            done();
        }).catch(err => {
            console.log('err :', err);
            done(err)
        });
    })

    it('clear ip address', done => {
        svc.clearIpAddress(in_data).then(data => {
            console.log('data :', data);
            done();
        }).catch(err => {
            console.log('err :', err);
            done(err)
        });
    })

    
})