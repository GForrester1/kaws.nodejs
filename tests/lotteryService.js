/**
 * Created by cj on 3/11/18.
 */
'use strict';

const svc = require('../app/services/lotteryService');
const shortid = require('shortid');

describe('testing lotteryService', () => {
    
    
    it('should add lottery entry', done => {
        const rand = shortid();
        
        const in_data = {
            ip_address: `ip-address-${rand}`,
            first_name: `fname-${rand}`,
            last_name: `fname-${rand}`,
            email: `email-${rand}@emagid.dev`,
            product_id: 104,
            campaign_id: 5,
            terms: true, 
            phone: `phone-${rand}`,
            address: `address-${rand}`,
            city: `city-${rand}`,
            zip: `zip-${rand}`,
            state: `state-${rand}`,
        }; 
        
        svc.createEntry(in_data ).then(data => {

            return svc.createEntry(in_data).then(() => {
                return done(new Error('should have failed creating a duplicate entry'))
            }).catch(err =>{
                done();    
            })
            
        }).catch(err => {
            done(err);
        });
    })
    
    it('Check dates ', done => {
        const date = new Date();
        const unix = date.getTime() / 1000; 
        
        console.log(date.getTime() / 1000 ) ;
        console.log (unix > 1524672000) ;
        console.log (unix < 1524672000) ;
        
        done();
        // console.log(date.getUTCDate() > new Date('April 25, 2018 15:00:00 GMT'));
        
        
    })
})