/**
 * Created by cj on 4/16/18.
 */
$(document).ready(function(){
    console.log('KAWS v0.0.01')

    $($('.thumbnails a')[0]).click();

    if ( $('#lotto_form').is(':visible') ) {
        $('.figure_info').css('top', '-50px');
    }
    
    $('#reset-password').on('click', function(e){
        e.preventDefault();
        $('#login-section').hide();
        $('#reset-password-section').show();
    });
    
    $('#go-back-login').on('click', function(e){
        e.preventDefault();
        $('#login-section').show();
        $('#reset-password-section').hide();
    });

    $('.wholesale_opt_in').on('change',function(){
        if($(this).is(':checked')){
            $('.wholesale_input').show().prop('disabled',false);
        } else {
            $('.wholesale_input').hide().prop('disabled',true);
        }
    });


    $('.show_cart').click(function () {
        $.post('/cart/updateCart',{dropdown:true},function (data) {
            if(data.status == 'success'){
                $('#cart_popout .cart_content').html(data.cartHtml).parent().show();
            }
        });
    });


    // FIGURE SWITCH
    $('.thumbnails a').click(function(e){
        e.preventDefault();
        $('.thumbnails img').removeClass('selected');
        $($(this).children('img')).addClass('selected');
        var src = $($(this).children('img')).attr('src');
        // var img = "../assets/img/" + src.substring(29, src.length);
        $('.figures').css('background-image', "url('" + src + "')");
    });


    // OVERLAY POPUPS
    $('footer a').click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        $('.' + id).fadeIn();
    });

    $('#lotto_button').click(function(e){
        $('.lotto.popup').fadeIn();
    });

    $('.off_overlay').click(function(){
        $('.popup').fadeOut();
    });


    // track order form
    $('.return_form .button').click(function(){
        $('#ticket').slideDown();
        $(this).slideUp();
    });


    //Submit Lotto Entry form
    // $(document).on('click', '#btnLotterySubmit', function() {
    //     let $elem = $('#lotto_form');
    //     let data = $elem.serialize()
    //
    //     $.post( "/api/lottery-entry", data, function( data ) {
    //         console.log( data.name ); // John
    //         console.log( data.time ); // 2pm
    //     }, "json");
    //
    //
    //     // console.log($elem.serialize());
    //
    // })
    $('#btnLotterySubmit').click(function(e) {
        e.preventDefault();
        var entrant = {
            first_name:     $('#lotto_form [name=first_name]').val(),
            last_name:      $('#lotto_form [name=last_name]').val(),
            email:          $('#lotto_form [name=email]').val(),
            phone:          $('#lotto_form [name=phone]').val(),
            address:        $('#lotto_form [name=address]').val(),
            city:           $('#lotto_form [name=city]').val(),
            zip:            $('#lotto_form [name=zip]').val(),
            state:          $('#lotto_form [name=state]').val(),
            country:        $('#lotto_form [name=country]').val(),
            // campaign_id:    $('#lotto_form [name=campaign_id]').val(),
            product_id:     $('#lotto_form [name=product_id]').val()
        }
        if($('#lotto_form [name=address2]').val() !== '' ){
            entrant.address2 = $('#lotto_form [name=address2]').val();
        }
        // let data = $('#lotto_form').serialize();
        if($('#lotto_form [name=terms]').is(':checked')){
            entrant.terms = true;
        }
        $.ajax({
            url: '/api/lottery-entry',
            data:JSON.stringify(entrant),
            type:"POST",
            contentType: 'application/json',
            success: function (data) {
                sessionStorage.lottery      = data.campaign_id;
                sessionStorage.notification = "You have successfully entered and will be notified if you win";
                buildNotification(1,sessionStorage.notification);
                console.log(data);
                $('.overlay_content').fadeOut();
                // location.reload();
            },
            error: function (data) {
                console.log(data);
                sessionStorage.notification = data.responseJSON.errorMessage;
                buildNotification(0,sessionStorage.notification);
            },
            dataType: 'json'});
    });

    // Zip code filtering
    $('#lotto_form [name=zip]').on('keyup',function () {
       var zip = $(this).val();
        zip = zip.replace(/[^0-9-]/g,'');
        $(this).val(zip);
    });


    //Terms Click
    $('.terms_click').click(function(){
        $('.lotto.popup').fadeOut();
        $('.terms.popup').fadeIn();
    });


    $('#checkBox').click(function(){
        if ( $('#checkBox:checked').length > 0 ) {
            $('#lotto_form .button').removeClass('disabled');
            $('#lotto_form .button').prop('disabled' , false);
        }else {
            $('#lotto_form .button').addClass('disabled');
            $('#lotto_form .button').prop('disabled' , true);
        }
    });

    $('.verify_terms p').click(function(){
        if ( $('#checkBox:checked').length > 0 ) {
            $('#lotto_form .button').addClass('disabled');
            $('#lotto_form .button').prop('disabled' , true);
            $('#checkBox').prop('checked', false);
        }else {
            $('#lotto_form .button').removeClass('disabled');
            $('#lotto_form .button').prop('disabled' , false);
            $('#checkBox').prop('checked', true);
        }
    });
});

//Track Order page
$(document).ready(function(){
    if($('.order_conf').length == 0){
        return true;
    }
    console.log("Track Order Page");
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
    var dateObj = new Date($('.date').html().substring(1, 11));
    var month = dateObj.getMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();

    newdate = monthNames[dateObj.getMonth()] + " " + day + " " + year;
    $('.date').html(newdate);


    //- select tag
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function(e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/

    $('#ticket').submit(function(e){
        e.preventDefault();
        var pdata = $(this).serialize();
        $.post('/orders/ticket',pdata,function (data) {
            console.log(data);
            if(data.state){
                buildNotification(1,'Your message has been received, we will get back to you as soon as possible');
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                $('.return_form').hide();
            }
        });
    });

    document.addEventListener("click", closeAllSelect);
});

function buildNotification(type,message)
{
    var alert = '';
    if(type = 'message' || type == true){
        alert = ['success','Notification:'];
    } else {
        alert = ['danger','An Error Occurred:'];
    }
    var formattedMsg = message;
    var html = '';
    if(Array.isArray(message)){
        formattedMsg = '<ul>';
        for(i = 0; i < m.length; i++){
            entry = m[i];
            formattedMsg += '<li>';
            formattedMsg += $entry;
            formattedMsg += '</li>';
        }
        formattedMsg += '</ul>';
    }
    html += "<div class='alert alert-"+alert[0]+"'>";
    html += "<strong>"+alert[1]+" </strong>";
    html += formattedMsg;
    html += "</div>";

    if($('.mainPageContainer > div.notification').length == 0){
        $('.mainPageContainer').append('<div class="notification">');
    }
    $('.mainPageContainer > div.notification').html(html);
}
//# sourceMappingURL=app.js.map
